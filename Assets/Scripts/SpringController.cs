using UnityEngine;

public class SpringController : MonoBehaviour
{
    public Rigidbody gameSphere;
    public Rigidbody springRigidBody;

    public float force;

    private bool _springPressed;
    private bool _springInProgress;
    private float _timeForSpringInProgress;
    private float _currentTimeForSpringInProgress;

    private void Start()
    {
        _currentTimeForSpringInProgress = _timeForSpringInProgress;
    }

    private void FixedUpdate()
    {
        if (_springPressed)
        {
            _springPressed = false;
            _springInProgress = true;
            return;
        }

        if (_springInProgress)
        {
            _currentTimeForSpringInProgress -= Time.deltaTime;

            if (!(_currentTimeForSpringInProgress <= 0)) return;

            _currentTimeForSpringInProgress = _timeForSpringInProgress;
            _springInProgress = false;
            return;
        }

        var colliders = Physics.OverlapBox(transform.position, new Vector3(0.3f, 0.3f, 0.1f));

        foreach (var coll in colliders)
        {
            if (coll.name != gameSphere.gameObject.name) continue;

            springRigidBody.AddForce(new Vector3(0, 0, force), ForceMode.Impulse);
            _springPressed = true;
        }
    }
}
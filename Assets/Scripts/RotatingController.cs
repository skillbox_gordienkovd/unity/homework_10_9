using UnityEngine;

public class RotatingController : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        var joint = gameObject.GetComponent<HingeJoint>();
        joint.axis = -joint.axis;
        
        var currentRenderer = gameObject.GetComponent<Renderer>();
        currentRenderer.material.color = Random.ColorHSV();

        var targetRenderer = other.gameObject.GetComponent<Renderer>();
        targetRenderer.material.color = Random.ColorHSV();
    }
}
